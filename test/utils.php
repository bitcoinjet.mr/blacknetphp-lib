<?php

namespace Blacknet\Test\Lib;

use PHPUnit\Framework\TestCase;
use Blacknet\Lib\Core\Utils;


class BlacknetUtilsTest extends TestCase
{
    public function testEncodeVarIntAndDecodeVarInt()
    {
        $l = 1000;
        $a = Utils::encodeVarInt($l);
        $e = Utils::decodeVarInt($a);
        $this->assertEquals($l, $e);
    }
}