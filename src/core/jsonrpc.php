<?php

namespace Blacknet\Lib\Core;

use Blacknet\Lib\Exception\BlacknetException;
use Blacknet\Lib\Core\Utils;
use Blacknet\Lib\Core\Body;
use function Blacknet\Lib\signature;
use Requests;

class JSONRPC extends Utils
{
    var $endpoint;
    var $serialize;
    function __construct($endpoint = "https://blnmobiledaemon.blnscan.io") {
        $this->endpoint = $endpoint;
        $this->serialize= new Serialize($this);
    }
    public function get(string $url) {
        $request = Requests::get($this->endpoint.$url);
        return new Body($request->status_code, $request->body);
    }
    public function post(string $url) {
       
    }
    public function getSeq(string $account) {
        $body = $this->get('/api/v1/walletdb/getsequence/'.$account);
        if($body->code === 200){
            return intval($body->body);
        }else{
            throw new BlacknetException($body->body);
        }
    }
    public function getFees(string $message) {
        $minTxFee = 0.001 * 1e8;
        if(empty($message)){
            return $minTxFee;
        }
        $normalSize  = 184;
        $messageSize = count(self::stringToArray($message));
        $total = $normalSize + $messageSize;
        return intval($minTxFee * (1 + $total/1000));
    }

    /**
     * @param array $data map[mnemonic=>string, fee=>int, amount=>int, message=>string, to=>string, from=>string, encrypted=>int]
     * @return Body a object
     */
    public function transfer(array $data){
        if (!empty($data["message"])) {
            $nfee = $this->getFees($data["message"]);
            if ($nfee > intval($data["fee"])) {
                $data["fee"] = $nfee;
            }
        }
        // serialize
        $serializedRes = $this->serialize->transfer($data);
        if ($serializedRes->code !== 200) {
            return $serializedRes;
        }
        $sign = signature($data["mnemonic"], $serializedRes->body);
        return $this->get(join("/", [
            "/api/v2/sendrawtransaction",
            $sign
        ]));
    }
    /**
     * @param array $data map[mnemonic=>string, fee=>int, amount=>int, to=>string, from=>string]
     * @return string string
     */
    public function lease(array $data){
        // serialize
        $serializedRes = $this->serialize->lease($data);
        if ($serializedRes->code !== 200) {
            return $serializedRes;
        }
        $sign = signature($data["mnemonic"], $serializedRes->body);
        return $this->get(join("/", [
            "/api/v2/sendrawtransaction",
            $sign
        ]));
    }
    /**
     * @param array $data map[mnemonic=>string, fee=>int, amount=>int, to=>string, from=>string, height=>int]
     * @return string string
     */
    public function cancelLease(array $data){
        // serialize
        $serializedRes = $this->serialize->cancelLease($data);
        if ($serializedRes->code !== 200) {
            return $serializedRes;
        }
        $sign = signature($data["mnemonic"], $serializedRes->body);
        return $this->get(join("/", [
            "/api/v2/sendrawtransaction",
            $sign
        ]));
    }
    /**
     * @param array $data map[mnemonic=>string, fee=>int, amount=>int, to=>string, from=>string, height=>int, withdraw=>int]
     * @return string string
     */
    public function withdrawFromLease(array $data){
        // serialize
        $serializedRes = $this->serialize->withdrawFromLease($data);
        if ($serializedRes->code !== 200) {
            return $serializedRes;
        }
        $sign = signature($data["mnemonic"], $serializedRes->body);
        return $this->get(join("/", [
            "/api/v2/sendrawtransaction",
            $sign
        ]));
    }
}