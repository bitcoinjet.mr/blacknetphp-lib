<?php

namespace Blacknet\Lib\Core\Transaction;
use Blacknet\Lib\Core\Utils;
use Blacknet\Lib\Core\Signature;
use Blacknet\Lib\Core\Hash;

class Transaction extends Utils{
    var $signature; //Signature
    var $from; //string
    var $seq; //uint32
    var $hash; //Hash
    var $fee; //uint64
    var $type; //uint8
    var $data; //array
    function __construct($from, $seq, Hash $hash, $fee, $type, array $data) {
        $this->signature = Signature::empty();
        $this->from = self::publickeyToHex(self::publickey($from));
        $this->seq = intval($seq);
        $this->hash = $hash;
        $this->fee = intval($fee);
        $this->type = intval($type);
        $this->data = $data;
    }
    public function serialize(){
        $signature = $this->signature->bytes();
        $from = self::stringToArray(self::hexToPublickey($this->from));
        $seq = self::toUint32Array($this->seq);
        $hash = $this->hash->bytes();
        $fee = self::toUint64Array($this->fee);
        $type = self::toUint8Array($this->type);
        $len = self::encodeVarInt(count($this->data));
        return array_merge(
            $signature,
            $from,
            $seq,
            $hash,
            $fee,
            $type,
            $len,
            $this->data
        );
    }
    public static function derialize(array $arr){
        $from = self::publickeyToHex(self::arrayToString(array_slice($arr, 64, 32)));
        $seq  = self::uint32ArrayToNumeric(array_slice($arr, 96, 4));
        $fee  = self::uint64ArrayToNumeric(array_slice($arr, 132, 8));
        $type = self::uint8ArrayToNumeric(array_slice($arr, 140, 1));
        $len  = self::decodeVarInt(array_slice($arr, 141));
        $data = array_slice($arr, count($arr)-$len);
        return new Transaction(
            $from,
            $seq,
            new Hash(array_slice($arr, 100, 32)),
            $fee,
            $type,
            $data
        );
    }
}
