<?php

namespace Blacknet\Lib\Core;
// use SplFixedArray;

class Body extends Utils{
    var $code; //int
    var $body; //string
    function __construct( $code, $body ) {
        $this->code = $code;
        $this->body = $body;
    }
    public function array(){
        return array(
            "code"=>$this->code,
            "body"=>$this->body
        );
    }
}